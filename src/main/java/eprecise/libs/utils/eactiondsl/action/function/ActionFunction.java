
package eprecise.libs.utils.eactiondsl.action.function;

@FunctionalInterface
public interface ActionFunction<T> {

    T execute() throws Exception;
}
