
package eprecise.libs.utils.eactiondsl.action.function;

import static java.util.stream.Collectors.toList;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Future;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;


public class ActionFunctionCallBuilder<T> {

    private final ActionFunction<T> procedure;

    private final Map<Predicate<Exception>, Consumer<Exception>> specificsErrorHandlers = new HashMap<>();

    private Consumer<Exception> errorHandler;

    private final Function<Supplier<T>, Future<T>> asyncRunner;

    private final List<Runnable> beforeCallbacks = new ArrayList<>();

    private final List<Consumer<T>> successCallbacks = new ArrayList<>();

    private final List<Runnable> afterCallbacks = new ArrayList<>();

    public ActionFunctionCallBuilder(ActionFunction<T> procedure, Function<Supplier<T>, Future<T>> asyncRunner, Consumer<Exception> errorHandler) {
        this(procedure, asyncRunner, errorHandler, Collections.emptyList(), Collections.emptyList(), Collections.emptyList(), Collections.emptyMap());
    }

    public ActionFunctionCallBuilder(ActionFunction<T> procedure, Function<Supplier<T>, Future<T>> asyncRunner, Consumer<Exception> errorHandler, List<Runnable> beforeCallbacks,
            List<Consumer<T>> successCallbacks, List<Runnable> afterCallbacks, Map<Predicate<Exception>, Consumer<Exception>> specificErrorHandler) {
        this.procedure = procedure;
        this.asyncRunner = asyncRunner;
        this.errorHandler = errorHandler;
        this.afterCallbacks.addAll(afterCallbacks);
        this.beforeCallbacks.addAll(beforeCallbacks);
        this.successCallbacks.addAll(successCallbacks);
        this.specificsErrorHandlers.putAll(specificErrorHandler);

    }

    public ActionFunctionCallBuilder<T> before(Runnable run) {
        this.beforeCallbacks.add(run);
        return this;
    }

    public ActionFunctionCallBuilder<T> onSuccess(Consumer<T> run) {
        this.successCallbacks.add(run);
        return this;
    }

    public ActionFunctionCallBuilder<T> after(Runnable run) {
        this.afterCallbacks.add(run);
        return this;
    }

    public ActionFunctionCallBuilder<T> onError(Consumer<Exception> errorHandler) {
        this.errorHandler = errorHandler;
        return this;
    }

    public ActionFunctionCallBuilder<T> onError(Predicate<Exception> consumePredicate, Consumer<Exception> consumer) {
        this.specificsErrorHandlers.put(consumePredicate, consumer);
        return this;
    }

    public <ET extends Exception> ActionFunctionCallBuilder<T> onError(Class<ET> type, Consumer<ET> consumer) {
        this.onError(e -> type.isInstance(e), e -> consumer.accept(type.cast(e)));
        return this;
    }

    public T sync() {
        try {
            this.beforeCallbacks.forEach(Runnable::run);
            final T result = this.procedure.execute();
            this.successCallbacks.forEach(c -> c.accept(result));
            return result;
        } catch (final Exception e) {
            final List<Consumer<Exception>> specficConsumers = this.specificsErrorHandlers.entrySet().stream().filter(kv -> kv.getKey().test(e)).map(kv -> kv.getValue()).collect(toList());
            if (specficConsumers.isEmpty()) {
                this.errorHandler.accept(e);
            } else {
                specficConsumers.forEach(c -> c.accept(e));
            }
        } finally {
            this.afterCallbacks.forEach(Runnable::run);
        }
        return null;
    }

    public Future<T> async() {
        return this.asyncRunner.apply(this::sync);
    }

}
