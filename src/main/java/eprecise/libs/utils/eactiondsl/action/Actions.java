
package eprecise.libs.utils.eactiondsl.action;

import java.util.concurrent.TimeUnit;


public final class Actions {

    public static Runnable waitFor(long amountMillis) {
        return waitFor(amountMillis, TimeUnit.MILLISECONDS);
    }

    public static Runnable waitFor(long amount, TimeUnit unit) {
        return () -> {
            try {
                unit.sleep(amount);
            } catch (final InterruptedException e) {
                e.printStackTrace();
            }
        };
    }

}
