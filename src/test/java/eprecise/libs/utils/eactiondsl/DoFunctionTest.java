
package eprecise.libs.utils.eactiondsl;

import static eprecise.libs.utils.eactiondsl.action.Actions.waitFor;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.io.IOException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import org.junit.Test;


public class DoFunctionTest {

    private static final int ONE_CALL = 1;

    private static final int NONE_CALL = 0;

    private static final String TAG_RESULT = "result";

    private static final String TAG_ACTION = "action";

    private static final String TAG_BEFORE = "before";

    private static final String TAG_AFTER = "after";

    private static final String TAG_SUCCESS = "success";

    private final CallbackStateRetainer retainer = new CallbackStateRetainer();

    @Test
    public void testDoSimpleSyncInvoke() {
        final String result = Do.it(() -> this.retainer.call(TAG_ACTION, TAG_RESULT)).sync();
        assertEquals(TAG_RESULT, result);
        assertEquals(ONE_CALL, this.retainer.getCalls(TAG_ACTION).size());
    }

    @Test
    public void testDoSimpleAsyncInvoke() throws InterruptedException, ExecutionException {
        final Future<String> future = Do.it(() -> this.retainer.call(TAG_ACTION, TAG_RESULT)).async();
        assertEquals(TAG_RESULT, future.get());
        assertEquals(ONE_CALL, this.retainer.getCalls(TAG_ACTION).size());
    }

    @Test
    public void testDoSimpleSyncBeforeInvoke() {
        final String result = Do.it(() -> this.retainer.call(TAG_ACTION, TAG_RESULT)).before(() -> this.retainer.call(TAG_BEFORE)).sync();
        assertEquals(TAG_RESULT, result);
        assertEquals(ONE_CALL, this.retainer.getCalls(TAG_BEFORE).size());
        assertEquals(ONE_CALL, this.retainer.getCalls(TAG_ACTION).size());
    }

    @Test
    public void testDoSimpleAsyncBeforeInvoke() throws InterruptedException, ExecutionException {
        final Future<String> future = Do.it(() -> this.retainer.call(TAG_ACTION, TAG_RESULT)).before(() -> this.retainer.call(TAG_BEFORE)).async();
        assertEquals(TAG_RESULT, future.get());
        assertEquals(ONE_CALL, this.retainer.getCalls(TAG_BEFORE).size());
        assertEquals(ONE_CALL, this.retainer.getCalls(TAG_ACTION).size());
    }

    @Test
    public void testDoSimpleSyncSuccessInvoke() {
        Do.it(() -> this.retainer.call(TAG_ACTION, TAG_RESULT)).onSuccess(r -> {
            this.retainer.call(TAG_SUCCESS);
            assertEquals(TAG_RESULT, r);
        }).sync();
        assertEquals(ONE_CALL, this.retainer.getCalls(TAG_SUCCESS).size());
        assertEquals(ONE_CALL, this.retainer.getCalls(TAG_ACTION).size());
    }

    @Test
    public void testDoSimpleAsyncSuccessInvoke() throws InterruptedException, ExecutionException {
        final Future<String> future = Do.it(() -> this.retainer.call(TAG_ACTION, TAG_RESULT)).onSuccess(r -> {
            this.retainer.call(TAG_SUCCESS);
            assertEquals(TAG_RESULT, r);
        }).async();
        assertEquals(TAG_RESULT, future.get());
        assertEquals(ONE_CALL, this.retainer.getCalls(TAG_SUCCESS).size());
        assertEquals(ONE_CALL, this.retainer.getCalls(TAG_ACTION).size());
    }

    @Test
    public void testDoSimpleSyncAfterInvoke() {
        final String result = Do.it(() -> this.retainer.call(TAG_ACTION, TAG_RESULT)).after(() -> this.retainer.call(TAG_AFTER)).sync();
        assertEquals(TAG_RESULT, result);
        assertEquals(ONE_CALL, this.retainer.getCalls(TAG_AFTER).size());
        assertEquals(ONE_CALL, this.retainer.getCalls(TAG_ACTION).size());
    }

    @Test
    public void testDoSimpleAsyncAfterInvoke() throws InterruptedException, ExecutionException {
        final Future<String> future = Do.it(() -> this.retainer.call(TAG_ACTION, TAG_RESULT)).after(() -> this.retainer.call(TAG_AFTER)).async();
        assertEquals(TAG_RESULT, future.get());
        assertEquals(ONE_CALL, this.retainer.getCalls(TAG_AFTER).size());
        assertEquals(ONE_CALL, this.retainer.getCalls(TAG_ACTION).size());
    }

    @Test
    public void testDoFullSyncInvoke() {
        Do.it(() -> this.retainer.call(TAG_ACTION, TAG_RESULT)).before(() -> this.retainer.call(TAG_BEFORE)).after(() -> this.retainer.call(TAG_AFTER)).onSuccess(r -> {
            this.retainer.call(TAG_SUCCESS);
            assertEquals(TAG_RESULT, r);
        }).sync();
        assertEquals(ONE_CALL, this.retainer.getCalls(TAG_BEFORE).size());
        assertEquals(ONE_CALL, this.retainer.getCalls(TAG_AFTER).size());
        assertEquals(ONE_CALL, this.retainer.getCalls(TAG_ACTION).size());
        assertEquals(ONE_CALL, this.retainer.getCalls(TAG_SUCCESS).size());
    }

    @Test
    public void testDoFullAsyncDelayedBodyInvoke() throws InterruptedException, ExecutionException {
        final Future<String> future = Do.it(() -> {
            Thread.sleep(100);
            return this.retainer.call(TAG_ACTION, TAG_RESULT);
        }).before(() -> this.retainer.call(TAG_BEFORE)).after(() -> this.retainer.call(TAG_AFTER)).onSuccess(r -> {
            this.retainer.call(TAG_SUCCESS);
            assertEquals(TAG_RESULT, r);
        }).async();
        Thread.sleep(10);
        assertEquals(ONE_CALL, this.retainer.getCalls(TAG_BEFORE).size());
        assertEquals(NONE_CALL, this.retainer.getCalls(TAG_ACTION).size());
        assertEquals(NONE_CALL, this.retainer.getCalls(TAG_AFTER).size());
        assertEquals(NONE_CALL, this.retainer.getCalls(TAG_SUCCESS).size());
        assertEquals(TAG_RESULT, future.get());
        assertEquals(ONE_CALL, this.retainer.getCalls(TAG_AFTER).size());
        assertEquals(ONE_CALL, this.retainer.getCalls(TAG_ACTION).size());
        assertEquals(ONE_CALL, this.retainer.getCalls(TAG_AFTER).size());
        assertEquals(ONE_CALL, this.retainer.getCalls(TAG_SUCCESS).size());
    }

    @SuppressWarnings("finally")
    @Test(expected = RuntimeException.class)
    public void testSyncDefaultErrorHandler() {
        Do.it(() -> {
            try {
                return TAG_RESULT;
            } finally {
                throw new Exception();
            }
        }).sync();
    }

    @SuppressWarnings("finally")
    @Test
    public void testSyncCustomErrorHandler() {
        final String result = Do.it(() -> {
            try {
                return TAG_RESULT;
            } finally {
                throw new Exception();
            }
        }).onError(e -> this.retainer.call(Exception.class.getName())).sync();
        assertNull(result);
        assertEquals(ONE_CALL, this.retainer.getCalls(Exception.class.getName()).size());
    }

    @SuppressWarnings("finally")
    @Test
    public void testAsyncCustomErrorHandler() throws InterruptedException, ExecutionException {
        final Future<?> future = Do.it(() -> {
            try {
                return TAG_RESULT;
            } finally {
                throw new Exception();
            }
        }).onError(e -> this.retainer.call(Exception.class.getName())).async();
        assertNull(future.get());
        assertEquals(ONE_CALL, this.retainer.getCalls(Exception.class.getName()).size());
    }

    @SuppressWarnings("finally")
    @Test
    public void testSyncSpecificErrorHandler() {
        final String result = Do.it(() -> {
            try {
                return TAG_RESULT;
            } finally {
                throw new IOException();
            }
        }).onError(IOException.class::isInstance, e -> this.retainer.call(IOException.class.getName())).sync();
        assertNull(result);
        assertEquals(ONE_CALL, this.retainer.getCalls(IOException.class.getName()).size());
    }

    @SuppressWarnings("finally")
    @Test(expected = IllegalArgumentException.class)
    public void testSyncSpecificErrorHandlerWithAnotherException() {
        Do.it(() -> {
            try {
                return TAG_RESULT;
            } finally {
                throw new IllegalArgumentException();
            }
        }).onError(IOException.class::isInstance, e -> this.retainer.call(IOException.class.getName())).sync();
    }

    @SuppressWarnings("finally")
    @Test
    public void testSyncSpecificErrorHandlerByType() {
        Do.it(() -> {
            try {
                return TAG_RESULT;
            } finally {
                throw new IOException();
            }
        }).onError(IOException.class, e -> this.retainer.call(IOException.class.getName())).sync();
        assertEquals(ONE_CALL, this.retainer.getCalls(IOException.class.getName()).size());
    }

    @Test
    public void testAsyncWait() throws InterruptedException, ExecutionException {
        final Future<String> future = Do.it(() -> this.retainer.call(TAG_ACTION, TAG_RESULT)).before(waitFor(100)).async();
        assertEquals(NONE_CALL, this.retainer.getCalls(TAG_ACTION).size());
        assertEquals(TAG_RESULT, future.get());
        assertEquals(ONE_CALL, this.retainer.getCalls(TAG_ACTION).size());
    }
}
