
package eprecise.libs.utils.eactiondsl;

import static eprecise.libs.utils.eactiondsl.action.Actions.waitFor;
import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import org.junit.Test;


public class DoProcedureTest {

    private static final int ONE_CALL = 1;

    private static final int NONE_CALL = 0;

    private static final String TAG_ACTION = "action";

    private static final String TAG_BEFORE = "before";

    private static final String TAG_AFTER = "after";

    private static final String TAG_SUCCESS = "success";

    private final CallbackStateRetainer retainer = new CallbackStateRetainer();

    @Test
    public void testDoSimpleSyncInvoke() {
        Do.it(() -> this.retainer.call(TAG_ACTION)).sync();
        assertEquals(ONE_CALL, this.retainer.getCalls(TAG_ACTION).size());
    }

    @Test
    public void testDoSimpleAsyncInvoke() throws InterruptedException, ExecutionException {
        final Future<?> future = Do.it(() -> this.retainer.call(TAG_ACTION)).async();
        future.get();
        assertEquals(ONE_CALL, this.retainer.getCalls(TAG_ACTION).size());
    }

    @Test
    public void testDoSimpleSyncBeforeInvoke() {
        Do.it(() -> this.retainer.call(TAG_ACTION)).before(() -> this.retainer.call(TAG_BEFORE)).sync();
        assertEquals(ONE_CALL, this.retainer.getCalls(TAG_BEFORE).size());
        assertEquals(ONE_CALL, this.retainer.getCalls(TAG_ACTION).size());
    }

    @Test
    public void testDoSimpleAsyncBeforeInvoke() throws InterruptedException, ExecutionException {
        final Future<?> future = Do.it(() -> this.retainer.call(TAG_ACTION)).before(() -> this.retainer.call(TAG_BEFORE)).async();
        future.get();
        assertEquals(ONE_CALL, this.retainer.getCalls(TAG_BEFORE).size());
        assertEquals(ONE_CALL, this.retainer.getCalls(TAG_ACTION).size());
    }

    @Test
    public void testDoSimpleSyncSuccessInvoke() {
        Do.it(() -> this.retainer.call(TAG_ACTION)).onSuccess(() -> this.retainer.call(TAG_SUCCESS)).sync();
        assertEquals(ONE_CALL, this.retainer.getCalls(TAG_SUCCESS).size());
        assertEquals(ONE_CALL, this.retainer.getCalls(TAG_ACTION).size());
    }

    @Test
    public void testDoSimpleAsyncSuccessInvoke() throws InterruptedException, ExecutionException {
        final Future<?> future = Do.it(() -> this.retainer.call(TAG_ACTION)).onSuccess(() -> this.retainer.call(TAG_SUCCESS)).async();
        future.get();
        assertEquals(ONE_CALL, this.retainer.getCalls(TAG_SUCCESS).size());
        assertEquals(ONE_CALL, this.retainer.getCalls(TAG_ACTION).size());
    }

    @Test
    public void testDoSimpleSyncAfterInvoke() {
        Do.it(() -> this.retainer.call(TAG_ACTION)).after(() -> this.retainer.call(TAG_AFTER)).sync();
        assertEquals(ONE_CALL, this.retainer.getCalls(TAG_AFTER).size());
        assertEquals(ONE_CALL, this.retainer.getCalls(TAG_ACTION).size());
    }

    @Test
    public void testDoSimpleAsyncAfterInvoke() throws InterruptedException, ExecutionException {
        final Future<?> future = Do.it(() -> this.retainer.call(TAG_ACTION)).after(() -> this.retainer.call(TAG_AFTER)).async();
        future.get();
        assertEquals(ONE_CALL, this.retainer.getCalls(TAG_AFTER).size());
        assertEquals(ONE_CALL, this.retainer.getCalls(TAG_ACTION).size());
    }

    @Test
    public void testDoFullSyncInvoke() {
        Do.it(() -> this.retainer.call(TAG_ACTION)).before(() -> this.retainer.call(TAG_BEFORE)).after(() -> this.retainer.call(TAG_AFTER)).onSuccess(() -> this.retainer.call(TAG_SUCCESS)).sync();
        assertEquals(ONE_CALL, this.retainer.getCalls(TAG_BEFORE).size());
        assertEquals(ONE_CALL, this.retainer.getCalls(TAG_AFTER).size());
        assertEquals(ONE_CALL, this.retainer.getCalls(TAG_ACTION).size());
        assertEquals(ONE_CALL, this.retainer.getCalls(TAG_SUCCESS).size());
    }

    @Test
    public void testDoFullAsyncDelayedBodyInvoke() throws InterruptedException, ExecutionException {
        final Future<?> future = Do.it(() -> {
            Thread.sleep(100);
            this.retainer.call(TAG_ACTION);
        }).before(() -> this.retainer.call(TAG_BEFORE)).after(() -> this.retainer.call(TAG_AFTER)).onSuccess(() -> this.retainer.call(TAG_SUCCESS)).async();
        Thread.sleep(10);
        assertEquals(ONE_CALL, this.retainer.getCalls(TAG_BEFORE).size());
        assertEquals(NONE_CALL, this.retainer.getCalls(TAG_ACTION).size());
        assertEquals(NONE_CALL, this.retainer.getCalls(TAG_AFTER).size());
        assertEquals(NONE_CALL, this.retainer.getCalls(TAG_SUCCESS).size());
        future.get();
        assertEquals(ONE_CALL, this.retainer.getCalls(TAG_AFTER).size());
        assertEquals(ONE_CALL, this.retainer.getCalls(TAG_ACTION).size());
        assertEquals(ONE_CALL, this.retainer.getCalls(TAG_AFTER).size());
        assertEquals(ONE_CALL, this.retainer.getCalls(TAG_SUCCESS).size());
    }

    @Test(expected = RuntimeException.class)
    public void testSyncDefaultErrorHandler() {
        Do.it(() -> {
            throw new Exception();
        }).sync();
    }

    @Test
    public void testSyncCustomErrorHandler() {
        Do.it(() -> {
            throw new Exception();
        }).onError(e -> this.retainer.call(Exception.class.getName())).sync();
        assertEquals(ONE_CALL, this.retainer.getCalls(Exception.class.getName()).size());
    }

    @Test
    public void testAsyncCustomErrorHandler() throws InterruptedException, ExecutionException {
        final Future<?> future = Do.it(() -> {
            throw new Exception();
        }).onError(e -> this.retainer.call(Exception.class.getName())).async();
        future.get();
        assertEquals(ONE_CALL, this.retainer.getCalls(Exception.class.getName()).size());
    }

    @Test
    public void testSyncSpecificErrorHandler() {
        Do.it(() -> {
            throw new IOException();
        }).onError(IOException.class::isInstance, e -> this.retainer.call(IOException.class.getName())).sync();
        assertEquals(ONE_CALL, this.retainer.getCalls(IOException.class.getName()).size());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testSyncSpecificErrorHandlerWithAnotherException() {
        Do.it(() -> {
            throw new IllegalArgumentException();
        }).onError(IOException.class::isInstance, e -> this.retainer.call(IOException.class.getName())).sync();
    }

    @Test
    public void testSyncSpecificErrorHandlerByType() {
        Do.it(() -> {
            throw new IOException();
        }).onError(IOException.class, e -> this.retainer.call(IOException.class.getName())).sync();
        assertEquals(ONE_CALL, this.retainer.getCalls(IOException.class.getName()).size());
    }

    @Test
    public void testAsyncWait() throws InterruptedException, ExecutionException {
        final Future<?> future = Do.it(() -> this.retainer.call(TAG_ACTION)).before(waitFor(100)).async();
        assertEquals(NONE_CALL, this.retainer.getCalls(TAG_ACTION).size());
        future.get();
        assertEquals(ONE_CALL, this.retainer.getCalls(TAG_ACTION).size());
    }
}
